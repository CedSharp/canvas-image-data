import MouseService from './mouse.js';
import ParticleGenerator from './generator.js';

class ParticleManager {
  constructor(canvasId) {
    this.running = false;
    this.anim = null;
    this.data = null;
    this.particles = [];
    this.onStart = () => { };
    this.onStop = () => { };

    this._createOffscreenCanvas();
    this._findCanvas(canvasId);
  }

  load(image) {
    const img = new Image();
    img.onload = () => this._onImageLoaded(img);
    img.src = image;
  }

  start() {
    this.running = true;
    this.animate();
    this.onStart();
  }

  stop() {
    this.running = false;
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    if (this.anim !== null) cancelAnimationFrame(this.anim);
    this.onStop();
  }

  animate() {
    if (!this.running) return;
    const mousePercent = this.mouse.getPercent(this.canvas.width, this.canvas.height);
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    for (let particle of this.particles) {
      particle.update(mousePercent);
      particle.render(this.ctx, this.canvas.width, this.canvas.height);
    }
    this.anim = requestAnimationFrame(() => this.animate());
  }

  _createOffscreenCanvas() {
    /** @type {HTMLCanvasElement} */
    this.fakeCanvas = document.createElement('canvas');
    this.fakeCanvas.width = 100;
    this.fakeCanvas.height = 100;
    this.fakeCtx = this.fakeCanvas.getContext('2d');
  }

  _findCanvas(canvasId) {
    /** @type {HTMLCanvasElement} */
    this.canvas = document.getElementById(canvasId);
    this.canvas.width = this.canvas.clientWidth;
    this.canvas.height = this.canvas.clientHeight;
    this.ctx = this.canvas.getContext('2d');
    this.mouse = new MouseService(this.canvas);

    this.ctx.fillStyle = 'white';
    this.ctx.font = '1.5rem sans-serif';
    this.ctx.fillText('Select an image above to start.', 16, 32);
  }

  _onImageLoaded(image) {
    this.stop();
    const data = this._getImageData(image);
    this.particles = ParticleGenerator.generate(data);
    this.start();
  }

  _getImageData(image) {
    this.fakeCtx.clearRect(0, 0, 100, 100);
    this.fakeCtx.drawImage(image, 0, 0, 100, 100);
    return this.fakeCtx.getImageData(0, 0, 100, 100);
  }
}

export default ParticleManager;