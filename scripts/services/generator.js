import Particle from "../entities/particle.js";

const cell = 0.01; // 100 particles can fit in width/height

class ParticleGenerator {
  static generate(data) {
    const particles = [];
    let offset = 0;
    for (let py = 0; py < data.height; py++)
      for (let px = 0; px < data.width; px++) {
        // Read color info for this specific pixel
        const r = data.data[py * data.width + px + offset];
        const g = data.data[py * data.width + px + offset + 1];
        const b = data.data[py * data.width + px + offset + 2];
        // const a = data.data[py * data.width + px + offset + 3];

        // If pixel is not white, it is a particle
        if (r < 250 || g < 250 || b < 250)
          particles.push(new Particle((px + .5) * cell, (py + .5) * cell));

        // We have to read 4 values, px is 1, we need another 3
        offset += 3;
      }
    return particles;
  }
}

export default ParticleGenerator;