const noop = () => { };

class Controls {
  constructor() {
    this.images = document.getElementById('image-selector');
    this.playEl = document.getElementById('play');
    this.stopEl = document.getElementById('stop');

    this.onImageChanged = noop;
    this.onPlay = noop;
    this.onStop = noop;

    this.images.addEventListener('change',
      ({ target: { value } }) => this.onImageChanged(value)
    );

    this.playEl.addEventListener('click', () => this.onPlay());
    this.stopEl.addEventListener('click', () => this.onStop());
  }

  play() {
    this.playEl.classList.add('hidden');
    this.stopEl.classList.remove('hidden');
  }

  stop() {
    this.playEl.classList.remove('hidden');
    this.stopEl.classList.add('hidden');
  }
}

export default Controls;