class MouseService {
  constructor(target) {
    this.elem = target;
    this.x = 0;
    this.y = 0;
    this.dx = 0;
    this.dy = 0;
    this.lastX = 0;
    this.lastY = 0;

    target.addEventListener('mousemove', ev => this.onMouseMove(ev));
  }

  getPercent(width, height) {
    return { x: this.x / width, y: this.y / height };
  }

  onMouseMove(ev) {
    this.lastX = this.x;
    this.lastY = this.y;
    this.x = ev.offsetX;
    this.y = ev.offsetY;
    this.dx = this.x - this.lastX;
    this.dy = this.y - this.lastY;
  }
};

export default MouseService;