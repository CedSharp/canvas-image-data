import { easeInQuad, easeOutQuad } from '../utils/easing.js';

const radius = 0.175;
const force = 0.02;

class Particle {
  constructor(x, y) {
    this.x = x;
    this.y = y;
    this.dx = { from: 0, to: 0, value: 0 };
    this.dy = { from: 0, to: 0, value: 0 };
    this.alpha = 0.5;
    this.size = 1;
  }

  update(mouse) {
    const dist = this.distanceTo(this.x, this.y, mouse.x, mouse.y);

    if (dist < radius) {
      // Find destination
      const reverseDist = (radius - dist);
      const direction = reverseDist >= 0 ? 1 : -1;
      const angle = this.angleTo(this.x + this.dx, this.y + this.dy, mouse.x, mouse.y);
      const distLeft = this.distanceTo(this.dx, this.dy, mouse.x, mouse.y);
      const move = this.lengthDir(
        Math.min(distLeft, force * direction),
        angle + Math.PI
      );
      // Assign values based on calculation
      const scale = reverseDist / radius;
      this.alpha = scale * 0.5 + 0.5;
      this.size = scale * 4 + 1;
      // Handle displacement
      this.dx = move.dx;
      this.dy = move.dy;
      // TODO: add easing
    }
    else {
      this.size = 1;
      this.alpha = 0.5;
      this.dx = 0;
      this.dy = 0;
    }
  }

  angleTo(x1, y1, x2, y2) {
    const dx = x2 - x1;
    const dy = y2 - y1;
    return Math.atan2(dy, dx);
    // const angle = theta * 100 / Math.PI;
  }

  distanceTo(x1, y1, x2, y2) {
    const dx = Math.abs(x2 - x1);
    const dy = Math.abs(y2 - y1);
    return Math.sqrt(dx * dx + dy * dy);
  }

  lengthDir(dist, angle) {
    return {
      dx: Math.cos(angle) * dist,
      dy: Math.sin(angle) * dist
    };
  }

  render(ctx, width, height) {
    ctx.fillStyle = 'rgba(255, 255, 255, ' + this.alpha + ')';
    ctx.beginPath();
    ctx.arc(
      (this.x + this.dx) * width,
      (this.y + this.dy) * height,
      this.size,
      0,
      Math.PI * 2
    );
    ctx.closePath();
    ctx.fill();
  }
}

export default Particle;