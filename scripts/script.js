import ParticleManager from './services/particle-manager.js';
import Controls from './services/controls.js';

const controls = new Controls();
const manager = new ParticleManager('canvas');

controls.onImageChanged = imagePath => manager.load(imagePath);
controls.onPlay = () => manager.start();
controls.onStop = () => manager.stop();

manager.onStart = () => controls.play();
manager.onStop = () => controls.stop();